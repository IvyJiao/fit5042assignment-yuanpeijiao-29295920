package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.ContactPerson; 
import fit5042.tutex.repository.entities.Property;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Stateless
public class JPAPropertyRepositoryImpl implements PropertyRepository {

    @PersistenceContext(unitName = "Assignment-ejbPU")
    private EntityManager entityManager;

    @Override
    public void addProperty(Property property) throws Exception {
    	List<Property> properties =  entityManager.createNamedQuery(Property.GET_ALL_QUERY_NAME).getResultList(); 
    	// get new id
        property.setCustomerId(properties.get(0).getCustomerId() + 1);
        entityManager.persist(property);
    }
     
    @Override
    public Property searchPropertyById(int id) throws Exception {
        Property property = entityManager.find(Property.class, id);
        property.getTags();
        return property;
    }

    @Override
    public List<Property> getAllProperties() throws Exception {
        return entityManager.createNamedQuery(Property.GET_ALL_QUERY_NAME).getResultList();
    }

    @Override
    public Set<Property> searchPropertyByContactPerson(ContactPerson contactPerson) throws Exception {
        contactPerson = entityManager.find(ContactPerson.class, contactPerson.getConactPersonId());
        contactPerson.getProperties().size();
        entityManager.refresh(contactPerson);

        return contactPerson.getProperties();
    }

    @Override
    public List<ContactPerson> getAllContactPeople() throws Exception {
        return entityManager.createNamedQuery(ContactPerson.GET_ALL_QUERY_NAME).getResultList();
    }

    @Override
    public void removeProperty(int customerId) throws Exception {
        Property property = this.searchPropertyById(customerId);

        if (property != null) {
            entityManager.remove(property);
        }
    }

    @Override
    public void editProperty(Property property) throws Exception {   
         try {
            entityManager.merge(property);
        } catch (Exception ex) {
            
        }
    }

	@Override
	public void addContactPerson(ContactPerson contact) throws Exception {
		// TODO Auto-generated method stub
		List<ContactPerson> contacts =  entityManager.createNamedQuery(ContactPerson.GET_ALL_QUERY_NAME).getResultList(); 
    	// get new id
		contact.setConactPersonId(contacts.get(0).getConactPersonId() + 1);
        entityManager.persist(contact);
		
	}
	
	@Override
	public ContactPerson searchContactById(int id) throws Exception {
		// TODO Auto-generated method stub
		ContactPerson contact = entityManager.find(ContactPerson.class, id);
        return contact;
	}

	@Override
	public void removeContactPerson(int id) throws Exception {
		// TODO Auto-generated method stub
		ContactPerson contact = this.searchContactById(id);

        if (contact != null) {
            entityManager.remove(contact);
        }
	}

	@Override
	public void editContactPerson(ContactPerson contact) throws Exception {
		// TODO Auto-generated method stub
		try {
            entityManager.merge(contact);
        } catch (Exception ex) {
            
        }
	}

	

}
