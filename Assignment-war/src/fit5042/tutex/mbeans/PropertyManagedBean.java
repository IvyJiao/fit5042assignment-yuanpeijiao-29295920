/*
 * To change this license header, choose License Headers in Project Properties.
 */
package fit5042.tutex.mbeans;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.entities.Address;
import fit5042.tutex.repository.entities.ContactPerson;
import fit5042.tutex.repository.entities.Property;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;


@ManagedBean(name = "propertyManagedBean")
@SessionScoped

public class PropertyManagedBean implements Serializable {

    @EJB
    PropertyRepository propertyRepository;

    /**
     * Creates a new instance of PropertyManagedBean
     */
    public PropertyManagedBean() {
    }

    public List<Property> getAllProperties() {
        try {
            List<Property> properties = propertyRepository.getAllProperties();
            return properties;
        } catch (Exception ex) {
            Logger.getLogger(PropertyManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void addProperty(Property property) 
    {
        try {
             propertyRepository.addProperty(property);
        } catch (Exception ex) {
            Logger.getLogger(PropertyManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addContact(ContactPerson contact) 
    {
        try {
             propertyRepository.addContactPerson(contact);
        } catch (Exception ex) {
            Logger.getLogger(PropertyManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Search a property by Id
     */
    public Property searchPropertyById(int id)
    {
        try {
            return propertyRepository.searchPropertyById(id);
        } catch (Exception ex) {
            Logger.getLogger(PropertyManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public ContactPerson searchContactById(int id)
    {
        try {
            return propertyRepository.searchContactById(id);
        } catch (Exception ex) {
            Logger.getLogger(PropertyManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public Set<Property> searchPropertyByContactPerson(ContactPerson contactPerson) 
    {
        try {
            return propertyRepository.searchPropertyByContactPerson(contactPerson);
        } catch (Exception ex) {
            Logger.getLogger(PropertyManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public List<ContactPerson> getAllContactPeople() throws Exception {
    	
    	try {
            return propertyRepository.getAllContactPeople();
        } catch (Exception ex) {
            Logger.getLogger(PropertyManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void removeProperty(int propertyId) 
    {
        try {
            propertyRepository.removeProperty(propertyId);
        } catch (Exception ex) {
            Logger.getLogger(PropertyManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void removeContact(int id) 
    {
        try {
            propertyRepository.removeContactPerson(id);
        } catch (Exception ex) {
            Logger.getLogger(PropertyManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editProperty(Property property)
    {
        try {
            String s = property.getAddress().getStreetNumber();
            Address address = property.getAddress();
            address.setStreetNumber(s);
            property.setAddress(address);
            
            propertyRepository.editProperty(property);
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(PropertyManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void editContact(ContactPerson contact)
    {
        try {
           
            propertyRepository.editContactPerson(contact);
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact Person has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(PropertyManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addProperty(fit5042.tutex.controllers.Property localProperty) {
    	
        //convert this newProperty which is the local property to entity property
        Property property = convertPropertyToEntity(localProperty);

        try {
            propertyRepository.addProperty(property);
        } catch (Exception ex) {
            Logger.getLogger(PropertyManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addContactPerson(fit5042.tutex.controllers.ContactPerson localContact) {
    	
        //convert this newProperty which is the local property to entity property
    	ContactPerson contact = new ContactPerson();
    	contact.setConactPersonId(localContact.getConactPersonId());
    	contact.setName(localContact.getName());
    	contact.setPhoneNumber(localContact.getPhoneNumber());

        try {
            propertyRepository.addContactPerson(contact);
        } catch (Exception ex) {
            Logger.getLogger(PropertyManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    private Property convertPropertyToEntity(fit5042.tutex.controllers.Property localProperty) {
    	
        Property property = new Property(); //entity
        
        property.setCustomerName(localProperty.getCustomerName());
        property.setIndustryType(localProperty.getIndustryType());
        property.setCustomerPhoneNum("0" + localProperty.getCustomerPhoneNum());
        property.setEmail(localProperty.getEmail());
        
        String streetNumber = localProperty.getStreetNumber();
        String streetAddress = localProperty.getStreetAddress();
        String suburb = localProperty.getSuburb();
        String postcode = localProperty.getPostcode();
        String state = localProperty.getState();
        Address address = new Address(streetNumber, streetAddress, suburb, postcode, state);
        property.setAddress(address);
        
        property.setCustomerId(localProperty.getCustomerId());
        property.setTags(localProperty.getTags());
        
        int conactPersonId = localProperty.getConactPersonId();
        String name = localProperty.getName();
        String  phoneNumber = localProperty.getPhoneNumber();
        String type = localProperty.getType();
        
        ContactPerson contactPerson = new fit5042.tutex.repository.entities.ContactPerson(conactPersonId, name, phoneNumber);
        
        if (contactPerson.getConactPersonId() == 0)
            contactPerson = null;
        property.setContactPerson(contactPerson);
        
        return property;
    }
    

}
