package fit5042.tutex.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.tutex.mbeans.PropertyManagedBean;
import fit5042.tutex.repository.entities.ContactPerson;

import javax.faces.bean.ManagedProperty;


@RequestScoped
@Named("addContact")
public class AddContact {
    @ManagedProperty(value="#{propertyManagedBean}") 
    PropertyManagedBean propertyManagedBean;
    
    private boolean showForm = true;

    private ContactPerson contact;
    
    PropertyApplication app;
    
    public void setContact(ContactPerson contact){
        this.contact = contact;
    }
    
    public ContactPerson getContact(){
        return contact;
    }
    
    public boolean isShowForm() {
        return showForm;
    }

    public AddContact() 
    {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app  = (PropertyApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "propertyApplication");
        
        //instantiate propertyManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        propertyManagedBean = (PropertyManagedBean) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "propertyManagedBean");
    }

    public void addContact(fit5042.tutex.controllers.ContactPerson localContact) {
        //this is the local property, not the entity
       try
       {
    	    
            //add this contact to db via EJB
            propertyManagedBean.addContactPerson(localContact);

            //refresh the list in PropertyApplication bean
            app.searchAllContact();
            //updatecontactListInPropertyApplicationBean();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("ContactPerson has been added succesfully"));
            //refresh the property list in propertyApplication bean
       }
       catch (Exception ex)
       {
           
       }
        showForm = true;
    }
   
}

