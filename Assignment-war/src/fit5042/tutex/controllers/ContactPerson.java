
package fit5042.tutex.controllers;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import fit5042.tutex.repository.entities.Property;

@RequestScoped
@Named(value = "contact")
public class ContactPerson implements Serializable {
	
	private int conactPersonId;
    private String name;
    private String phoneNumber;
    
    private Set<Property> properties;

    public ContactPerson() {
    }

    public ContactPerson(int conactPersonId, String name, String phoneNumber) {
        this.conactPersonId = conactPersonId;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.properties = new HashSet<>();
    }

	public int getConactPersonId() {
		return conactPersonId;
	}

	public void setConactPersonId(int conactPersonId) {
		this.conactPersonId = conactPersonId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Set<Property> getProperties() {
		return properties;
	}

	public void setProperties(Set<Property> properties) {
		this.properties = properties;
	}

	@Override
	public String toString() {
		return "ContactPerson [conactPersonId=" + conactPersonId + ", name=" + name + ", phoneNumber=" + phoneNumber
				+ ", properties=" + properties + "]";
	}
    
}

