/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.controllers;

import fit5042.tutex.repository.entities.Address;
import fit5042.tutex.repository.entities.ContactPerson;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named(value = "property")
public class Property implements Serializable {
    private int customerId;
    private String customerName;
    private String industryType;
    private String customerPhoneNum;
    private String email;

    private Address address;
    private ContactPerson contactPerson;

    private String streetNumber;
    private String streetAddress;
    private String suburb;
    private String postcode;
    private String state;
    
    private int conactPersonId;
    private String name;
    private String phoneNumber;
    private String type;
    
    private HashSet<String> tags;
    
    private Set<fit5042.tutex.repository.entities.Property> properties;
    
        public Property() {
        this.tags = new HashSet<>();
    }

    //non-defaut constructor
    public Property(int customerId, String customerName, String industryType, String customerPhoneNum, String email,
    		Address address, ContactPerson contactPerson, HashSet<String> tags) {
        this.customerId = customerId;
        this.customerName = customerName;
		this.industryType = industryType;
		this.customerPhoneNum = customerPhoneNum;
		this.email = email;
		
        this.address = address;
        this.contactPerson = contactPerson;
        this.tags = tags;
    }

    //=================================================================================
    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

  //=================================================================================
    public int getConactPersonId() {
        return conactPersonId;
    }

    public void setConactPersonId(int conactPersonId) {
        this.conactPersonId = conactPersonId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	//=================================================================================
    public Set<fit5042.tutex.repository.entities.Property> getProperties() {
        return properties;
    }

    public void setProperties(Set<fit5042.tutex.repository.entities.Property> properties) {
        this.properties = properties;
    }
    //==================================================================================

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public String getCustomerPhoneNum() {
		return customerPhoneNum;
	}

	public void setCustomerPhoneNum(String customerPhoneNum) {
		this.customerPhoneNum = customerPhoneNum;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    
    public ContactPerson getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(ContactPerson contactPerson) {
        this.contactPerson = contactPerson;
    }


    public HashSet<String> getTags() {
        return tags;
    }

    public void setTags(HashSet<String> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return "Property{" + "customerId=" + customerId + "," + ", customerName=" + customerName + ", industryType="
				+ industryType + ", customerPhoneNum=" + customerPhoneNum + ", email=" + email
        		+ "address=" + address + ", contactPerson=" + contactPerson + ", tags=" + tags + '}';
    }
}

