package fit5042.tutex.controllers;

import javax.el.ELContext; 
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.tutex.mbeans.PropertyManagedBean;
import fit5042.tutex.repository.entities.ContactPerson;

import javax.faces.bean.ManagedProperty;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

@RequestScoped
@Named("removeContact")
public class RemoveContact {
    @ManagedProperty(value="#{propertyManagedBean}") 
    PropertyManagedBean propertyManagedBean;
    
    private boolean showForm = true;
    private ContactPerson contact;
    PropertyApplication app;
    
    public void setContact(ContactPerson contact){
        this.contact = contact;
    }
    
    public ContactPerson getContact(){
        return contact;
    }
    
    public boolean isShowForm() {
        return showForm;
    }

    public RemoveContact() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (PropertyApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "propertyApplication");
        
        app.updateContactList();
        
        //instantiate propertyManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        propertyManagedBean = (PropertyManagedBean) FacesContext.getCurrentInstance().getApplication()
        .getELResolver().getValue(elContext, null, "propertyManagedBean");
    }

    /**
     * @param contactPeople Id 
     */
    public void removeContact(int id) {
       try
       {
            
            propertyManagedBean.removeContact(id);
            app.searchAllContact();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact has been deleted succesfully"));     
       }
       catch (Exception ex)
       {
           
       }
       showForm = true;

    }
 
}

