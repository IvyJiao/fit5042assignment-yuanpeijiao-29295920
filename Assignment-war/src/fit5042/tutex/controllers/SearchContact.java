package fit5042.tutex.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.tutex.repository.entities.ContactPerson;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

@RequestScoped
@Named("searchContact")
public class SearchContact {
    private ContactPerson contact;
    
    PropertyApplication app;
    
    private int searchByInt;

    public PropertyApplication getApp() {
        return app;
    }

    public void setApp(PropertyApplication app) {
        this.app = app;
    }

    public int getSearchByInt() {
        return searchByInt;
    }

    public void setSearchByInt(int searchByInt) {
        this.searchByInt = searchByInt;
    }

   
    public SearchContact() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (PropertyApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "propertyApplication");
        
        app.updateContactList();
    }

    /**
     * Normally each page should have a backing bean but you can actually do it
     * any how you want.
     *
     * @param contact Id 
     */
    public void searchContactById(int id) 
    {
       try
       {
            //search this contact then refresh the list in PropertyApplication bean
            app.searchContactById(id);
       }
       catch (Exception ex)
       {
           
       }
    }
    
    public void searchAllContact() 
    {
       try
       {
            //return all Contacts from db via EJB
             app.searchAllContact();
       }
       catch (Exception ex)
       {
           
       }
    }
    
}

