package fit5042.tutex.controllers;

import javax.el.ELContext;
import javax.inject.Named;

import fit5042.tutex.repository.entities.ContactPerson;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

@Named(value = "contactController")
@RequestScoped
public class ContactController {

    private ContactPerson contact;
    private int contactId;

    public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public ContactController() {
        //Contact
        contactId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("contactID"));
        contact = getContact();
    }

    public ContactPerson getContact() {
		// TODO Auto-generated method stub
    	
    	if (contact == null) {
            // Get application context bean PropertyApplication 
            ELContext context
                    = FacesContext.getCurrentInstance().getELContext();
            PropertyApplication app
                    = (PropertyApplication) FacesContext.getCurrentInstance()
                            .getApplication()
                            .getELResolver()
                            .getValue(context, null, "propertyApplication");
            // -1 to propertyId since we +1 in JSF (to always have positive property id!) 
            return app.getContacts().get(--contactId); 
            }
        return contact;
	}
	
}
