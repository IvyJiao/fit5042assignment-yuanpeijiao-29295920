package fit5042.tutex.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
    @NamedQuery(name = Property.GET_ALL_QUERY_NAME, query = "SELECT p FROM Property p order by p.customerId desc")})


public class Property implements Serializable {

    public static final String GET_ALL_QUERY_NAME = "Property.getAll";

    private int customerId;
    private String customerName;
    private String industryType;
    private String customerPhoneNum;
    private String email;
    
    private Address address;
    private String streetNumber;
	private String streetAddress;
	private String suburb;
	private String postcode;
	private String state;
    
    private ContactPerson contactPerson;

    private Set<String> tags;

    public Property() {
        this.tags = new HashSet<>();
    }

    public Property(int customerId, String customerName, String industryType, String customerPhoneNum, String email, 
			Address address, ContactPerson contactPerson, Set<String> tags) {
    	this.customerId = customerId;
		this.customerName = customerName;
		this.industryType = industryType;
		this.customerPhoneNum = customerPhoneNum;
		this.email = email;
        
		this.address = address;
        this.contactPerson = contactPerson;
        this.tags = tags;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "customer_id")
    public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public String getCustomerPhoneNum() {
		return customerPhoneNum;
	}

	public void setCustomerPhoneNum(String customerPhoneNum) {
		this.customerPhoneNum = customerPhoneNum;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
    
    
//----------------------------------------------------------------------------------------------------------------
    @Embedded
    public Address getAddress() {
        return address;
    }
    
	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getSuburb() {
		return suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setAddress(Address address) {
        this.address = address;
    }

    @ManyToOne
    public ContactPerson getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(ContactPerson contactPerson) {
        this.contactPerson = contactPerson;
    }

    @ElementCollection
    @CollectionTable(name = "tag")
    @Column(name = "value")
    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

	@Override
	public String toString() {
		return "Property [customerId=" + customerId + ", customerName=" + customerName + ", industryType="
				+ industryType + ", customerPhoneNum=" + customerPhoneNum + ", email=" + email + ", address=" + address
				+ "]";
	}

   
}
